package methods

object CallByNameMethod {

  def main(args: Array[String]): Unit = {
    doSomething(something())
  }

  // This method gets as argument another method!
  def doSomething(methodThatReturnsString: => String) = {
    println("In doSomething method, parameter: " + methodThatReturnsString)
    methodThatReturnsString
  }

  def something() = {
    println("In something method")
    "something!"
  }

  def useGivenFunctionThatReceivesAStringAndReturnsAString(f: String => String) : Unit = {
    val returnedString = f("astring")
    println(returnedString)
  }

}
