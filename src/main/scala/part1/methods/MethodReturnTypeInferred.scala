package methods

object MethodReturnTypeInferred {

  def main(args: Array[String]) : Unit = {
    val x = doo()
    println("Method doo returned '" + x + "' with type " + x.getClass)
  }

  def doo() = {
    "doo!"
  }

}
