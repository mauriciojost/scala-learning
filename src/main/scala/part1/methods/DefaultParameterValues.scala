package methods

object DefaultParameterValues {

  def main(args: Array[String]): Unit = {
    foo()
    foo("simpleGiven")
    foo(parameter="explicitelyGiven")
  }

  def foo(parameter: String = "default") = {
    println(parameter)
  }

}
