package methods

object BasicPerson {
  
  // In scala: constants are declared using "val", variables with "var" and methods with "def"

  // Values that remain unchanged (constants) are declared with "val"
  val _humanDna: String = "..."

  // Object properties use the Hungarian notation (prepend the "_")
  var _name: String = ""

  // Accessor of a property, does not use "get" as in Java, just the propertyName
  def name = _name

  // Mutator of a property, does not use "set" as in Java
  def name_=(n: String) = {
    _name = n
  }

  def howToReturnAValueWithReturnStatement(a: String) : String = {
    return "returnWithReturnStatement"
  }

  def howToReturnAValueWithoutReturnStatement(a: String) : String = {
    "returnWithtoutReturnStatement"
  }

  def iDoNotReturnAnything() : Unit = {
    println("Unit = void")
  }

  def iDoNotreturnAnythingEither() = {
    println("(no explicit return) = Unit = void")
  }

}
