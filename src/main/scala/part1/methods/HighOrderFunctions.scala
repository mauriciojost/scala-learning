package methods

object HighOrderFunctions {

  // Functions that use other functions

  def main(args: Array[String]) {

    // Both are exactly the same
    println(highOrderFunction1(doo, 10))
    println(highOrderFunction2(doo, 10))

  }

  def highOrderFunction1(functionParameter: Int => String, integer: Int) = functionParameter(integer)

  def highOrderFunction2(functionParameter: Int => String, integer: Int) = {
    functionParameter(integer)
  }

  def doo[A](argument: A) = "return " + argument.toString()

}

