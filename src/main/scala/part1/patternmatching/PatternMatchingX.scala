package patternmatching

object PatternMatchingX {
  /*
                     Scala.Any
                 /              \
                /                \
          AnyVal (primitive?)      AnyRef (non-primitive, woudl be an Object in java)
          / |    | \               /  |   \
         /  |    |  \             /   |    String
        /   |    |   Unit        Seq  |
    Double  |    Boolean            \ |
       Int                      List
         ...                        ...
                                | /  /
           ...
          \    \   |  |         /
             \  \  |  |      Null
                 \ \          /
                     Nothing
  */

  case class Person(name: String, age: Int)

  def main(args: Array[String]): Unit = {

    println("Match an integer:   " + matchTest(1))
    println("Match a string:     " + matchTest("two"))
    println("Match a case class: " + matchTest(Person("Pepe", 18)))
    println("Match nothing:      " + matchTest("xx"))

  }

  def matchTest(x: Any): Any = {

    x match {
      case 1 => "one"
      case "two" => 2
      case y: Int => "scala.Int"
      case Person(name: String, age: Int) => name + age
      case _ => "?"
    }

  }

}
