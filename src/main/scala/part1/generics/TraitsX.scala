package generics

// Only a class can be generic, its instantiation proceeds once a type is provided
object TraitsX {

  trait ParentTrait {def parentField: Int}
  trait ChildTrait extends ParentTrait {def childField: Int}
  trait BadChildTrait extends ParentTrait {def badChildField: Int}

  case class ReaderForParentTrait[F <: ParentTrait](foo: F) { // Parameter extends from ParentalTrait
    def readParentField(implicit ev: F <:< ParentTrait) = foo.parentField
  }

  // TODO how to read this?
  case class ReaderForChildTrait[+F <: ParentTrait](foo: F) { // This means: the parent of F is ParentTrait?
    def readChildField(implicit ev: F <:< ChildTrait) = foo.childField // This means: assume F is ChildTrait
  }

  def main(args: Array[String]) : Unit = {

    val t = new ChildTrait {
      override def parentField: Int = 10
      override def childField: Int = 20
    }

    println("ReaderForParentTrait.readParentField: " + ReaderForParentTrait(t).readParentField)
    println("ReaderForChildTrait.readChildField: " + ReaderForChildTrait(t).readChildField)


    /**

    THIS WILL FAIL (pass a child of ParentTrait that is not ChildTrait, but BadChildTrait):

    val t2 = new BadChildTrait {
      override def parentField: Int = 10
      override def badChildField: Int = 20
    }

    println("ReaderForChildTrait.readChildField*: " + ReaderForChildTrait(t2).readChildField

    WITH THE ORIGINAL ERROR:
    Error:(20, 79) Cannot prove that generics.TraitsX.BadChildTrait <:< generics.TraitsX.ChildTrait.
    println("ReaderForChildTrait.readChildField*: " + ReaderForChildTrait(t2).readChildField)

    "A <:< B" means "A is a sub-type of B" of "A can be casted to B"
    ORIGINAL ERROR

    */

  }

  /*
  To learn:
  - evidence instance  <:<   allows implicit conversion from B to Bar[ReadableFoo]
  -

  covariant
  A covariant annotation can be applied to a type parameter of a class or trait
  by putting a plus sign (+) before the type parameter.
  The class or trait then subtypes covariantly within the same direction asthe type annotated parameter.
  For example, List is covariant in its type parameter, so List[String] is a subtype of List[Any].

  Mauri:
  if List[String] is List



   */

}
