package generics

// Only a class can be generic, its instantiation proceeds once a type is provided
trait Generics[A] {

  var attribute : A

  def storedAttritube(att: A) : Unit = {
    attribute = att

  }

  def storedAttritube() : A = {
    attribute
  }

  /*
  def aMethodThatIsGenericWithConstraintInTheGenericType[B : String](o : B) : B = {
    o
  }

  def aMethodX() : Unit = {
    val string = ""
    aMethodThatIsGenericWithConstraintInTheGenericType(string); // OK

    val integer = 18
    aMethodThatIsGenericWithConstraintInTheGenericType(integer); // NOT OK
  }
  */

}
