package general


object Person {

  // In scala: constants are declared with "val", variables with "var" and methods with "def"

  // Values that remain unchanged (constants) are declared with "val"
  val HumanDna: String = "..."

  // Object properties use the Hungarian notation (prepend the "_")
  var Name: String = ""

  def howToReturnAValueWithReturnStatement(a: String) : String = {
    return "returnWithReturnStatement"
  }

  def howToReturnAValueWithoutReturnStatement(a: String) : String = {
    "returnWithtoutReturnStatement"
  }


}
