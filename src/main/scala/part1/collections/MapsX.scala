package collections

object MapsX {
  def main(args: Array[String]): Unit = {

    var m = Map("a" -> "A")


    println("Map: " + m)

    m += ("b" -> "B") // this will create a new immutable Map
    m += ("c" -> "C")

    println("Map: " + m)

    println("Map(c): " + m("c"))
  }
}
