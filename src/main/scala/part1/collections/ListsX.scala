package collections

object ListsX {



  def main(args: Array[String]): Unit = {
    val a = List(1, 2, 3)
    val b = List(1, 2, 3)

    println("Concatenated lists: " + (a ::: b))

    val numbers = 1 :: (2 :: (3 :: (4 :: Nil)))

    println("Concatenated numbers: " + numbers)

    println("Head: " + numbers.head)

    println("Tail (all the list but the head element): " + numbers.tail)

    println("Fill usage with argument 3 and x: " + List.fill(3)("x"))

    val squares = List.tabulate(6)(n => n * n)
    println("Tabulate use with n squared: " + squares)

    // Tabulate
    // 2 (0, 1)   *   4 (0, 1, 2, 3)
    // 0 * (0 1 2 3) = (0, 0, 0, 0)
    // 1 * (0 1 2 3) = (0, 1, 2, 3)
    val mul = List.tabulate(2, 4)(_ * _)
    println("Tabulate use with *: " + mul)
    
  }
}
