package collections

object SetsX {

  // No duplicate elements

  def main(args: Array[String]): Unit = {

    val setOdd: Set[Int] = Set(1, 3, 5, 7) // not mutable
    var setEve: Set[Int] = Set(2, 4, 6, 8) // mutable

    println("setOdd: " + setOdd)
    println("setOdd-1: " + (setOdd-1))  // Unchanged (not mutable)
    println("setOdd+1: " + (setOdd+1))  // Unchanged (not mutable)
    println("setOdd+9: " + (setOdd+9))  // Unchanged (not mutable)
    println("setOdd: " + (setOdd))      // Unchanged (not mutable)
    println("setOdd.max: " + setOdd.max) // Unchanged

    println("setEve: " + setEve)
    setEve+=10
    println("setEve (after += 10): " + setEve)  // Changed (mutable)

    println("setOdd++setEve: " + (setOdd++setEve))

  }

}
