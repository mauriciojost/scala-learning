package loops

import scala.util.control.Breaks

object Loops {
  def main(args: Array[String]): Unit = {
    // with a range
    for (i <- 1 to 10) {
      println(i)
    }

    // Iterating over a list
    val numList = List(1, 2, 3, 4, 5, 6);
    // for loop execution with a collection
    for (i <- numList) {
      println("Value of i: " + i);
    }

    // Iterating over a list with a filter on even numbers
    for (i <- numList if i % 2 == 0) {
      println("Filtered value of i: " + i);
    }

    val returnedListWithValuesUsingYield = for {
      i <- numList if i % 2 == 1
    } yield i
    println("Yield values: " + returnedListWithValuesUsingYield)

    // Iterating using the foreach method of lists
    numList.foreach { a =>
      println("Foreach item: " + a)
    }

    val loop = new Breaks;
    loop.breakable {
      for (i <- 1 to 10) {
        if (i < 5) {
          println("Before the break: " + i)
        } else {
          loop.break;
        }
      }
    }

  }
}
