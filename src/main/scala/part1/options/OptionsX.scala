package options

object OptionsX {



  def main(args: Array[String]): Unit = {

    var option: Option[String] = ???

    // An option can either contain another object
    option = Some("string")

    // or be empty
    option = None

    // Note that None is the equivalent to Null in java!!!

  }
}
