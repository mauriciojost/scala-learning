package blocksandtuples

object Blocks {

  def main(args: Array[String]): Unit = {

    val aTuple = (1, 2, 3)

    println("Tuple: " + aTuple)
    println("Tuple class: " + aTuple.getClass)
    println("Tuple element: " + aTuple._1)
    println("Tuple element: " + aTuple._2)
    println("Tuple element: " + aTuple._3)

    aTuple.productIterator.foreach { x =>
      println("Foreach item: " + x)
    }

  }

}
