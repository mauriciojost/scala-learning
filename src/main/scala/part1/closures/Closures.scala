package closures

object Closures {

  // A closure is simply a function (so no side effects).
  // It is special with regards to a regular function because:
  // - it takes parameters from parameter list but also from the current
  //   scope (as "y" in the example)
  // -

  def main(args: Array[String]) {

    val y = "yyy"

    val closureThatReturnsStringUsesXandY = (x: String) => x + y
    def methodThatReturnsStringUsesOnlyX(x: String): String = {x}

    println(closureThatReturnsStringUsesXandY("xxx"))
    println(methodThatReturnsStringUsesOnlyX("xxx"))

  }

}
