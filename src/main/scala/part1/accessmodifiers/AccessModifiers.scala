package accessmodifiers

object AccessModifiers {

  // Only private, protected and default(public) access modifiers for members
  class Outer {

    class Inner1 {
      private def privMethod() {
        println("f")
      }

      def pubMethod() {
        println("f")
      }

      protected def protMethod() {
        println("f")
      }

      class InnerMost {
        privMethod() // OK
        protMethod() // OK
        pubMethod() // OK
      }

    }

    class Inner2 {

      class InnerMost {
        val a = new Inner1()
        a.pubMethod() // OK
        //a.protMethod() // NOT ACCESSIBLE
        //a.privMethod() // NOT ACCESSIBLE
      }

    }

  }

}
