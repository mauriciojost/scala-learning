package identifiers

object Identifiers {
  def main(args: Array[String]) : Unit = {

    // In Scala, identifiers are made of two namespaces: type and name
    // In Java, identifiers are made of one namespace: name

    class X                     // type X, name X
    val X = 1                   // type int, name X

    println(new X)              // returns new object
    println(X)                  // returns 1

  }
}
